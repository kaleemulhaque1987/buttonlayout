import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button-layout',
  templateUrl: './button-layout.component.html',
  styleUrls: ['./button-layout.component.css']
})
export class ButtonLayoutComponent implements OnInit {
  @Output() firstLayout = new EventEmitter();
  firstLayoutVisible=true;
  secondLayoutVisible=true;
  thirdLayoutVisible=true;
  fourLayoutVisible=true;
  currentLayout={"currentComponent":"",show:{}};
  constructor() { }

  ngOnInit() {
    
  }
  firstButton() {
    console.log("clicked on first");
    this.currentLayout.currentComponent="first";
    console.log("onclicking",this.firstLayoutVisible);
    this.firstLayoutVisible=!this.firstLayoutVisible;
    this.currentLayout.show=this.firstLayoutVisible;
    this.firstLayout.emit(this.currentLayout);
  }
  secondButton() {
    console.log("clicked again on second");
    this.currentLayout.currentComponent="second";
    this.secondLayoutVisible=!this.secondLayoutVisible;
    this.currentLayout.show=this.secondLayoutVisible;
    this.firstLayout.emit(this.currentLayout);
  }
  thirdButton() {
    console.log("clicked again on third");
    this.currentLayout.currentComponent="third";
    this.thirdLayoutVisible=!this.thirdLayoutVisible;
    this.currentLayout.show=this.thirdLayoutVisible;
    this.firstLayout.emit(this.currentLayout);
  }
  
  fourButton() {
    console.log("clicked again on four");
    this.currentLayout.currentComponent="four";
    this.fourLayoutVisible=!this.fourLayoutVisible;
    this.currentLayout.show=this.fourLayoutVisible;
    this.firstLayout.emit(this.currentLayout);
  }
}
