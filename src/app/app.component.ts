import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { ButtonLayoutComponent } from './button-layout/button-layout.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isFirstVisible=true;
  isSecondVisible=true;
  isThirdVisible=true;
  isFourVisible=true;
  

  constructor(private cdRef:ChangeDetectorRef,_router:Router) {

    

  }

  
  
  ngOnInit() {

  }
  
  
  
  
  firstBtn(event) {
  console.log("event",event);
  //this.isFirstVisible=event;
  if(event.currentComponent =="first") {
    this.isFirstVisible =event.show;
  }else if(event.currentComponent=="second") {
    this.isSecondVisible=event.show;
  }else if(event.currentComponent=="third") {
    this.isThirdVisible=event.show;
  }else if(event.currentComponent=="four") {
    this.isFourVisible=event.show;
  }
  }
}
